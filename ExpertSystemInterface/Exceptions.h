/*
 * Exceptions.h
 *
 *  Created on: May 28, 2013
 *      Author: avolio
 */

#ifndef DAQ_ES_EXCEPTIONS_H_
#define DAQ_ES_EXCEPTIONS_H_

#include <ers/ers.h>

#include <string>

namespace daq { namespace es {

    ERS_DECLARE_ISSUE(ESInterface, Exception, ERS_EMPTY, ERS_EMPTY)

    ERS_DECLARE_ISSUE_BASE
    (ESInterface,
     CannotNotify,
     ESInterface::Exception,
     "The \"" << es_name << "\" Expert System cannot be properly contacted: " << message,
     ERS_EMPTY,
     ((std::string) es_name)   // Name of the ES
     ((std::string) message)   // Failure reason
    )
}}


#endif /* DAQ_ES_EXCEPTIONS_H_ */
