/*
 * RemoteES.h
 *
 *  Created on: May 28, 2013
 *      Author: avolio
 */

#ifndef DAQ_ES_REMOTEES_H_
#define DAQ_ES_REMOTEES_H_

#include "ExpertSystemInterface/ExpertSystemInterface.h"

#include <es/es.hh>
#include <ipc/partition.h>

#include <string>
#include <map>
#include <memory>


namespace daq { namespace es { namespace internal {
    class Utils;
}}}

namespace daq { namespace es {

    std::string centralESName();

namespace rc {
    class RemoteES : public ExpertSystemInterface {
        public:
            RemoteES(const std::string& partitionName, const std::string& esName);

            // throws daq::es::ESInterface::CannotNotify
            void applicationUpdate(const std::string& appName, const ProcessInfo& info) override;

            // throws daq::es::ESInterface::CannotNotify
            void applicationUpdate(const std::string& appName, const StateInfo& info) override;

            // throws daq::es::ESInterface::CannotNotify
            void applicationUpdate(const std::string& appName, const TestInfo& info) override;

            // throws daq::es::ESInterface::CannotNotify
            void applicationUpdate(const std::map<std::string, StateInfo>& infoList) override;

            // throws daq::es::ESInterface::CannotNotify
            void applicationUpdate(const std::map<std::string, ProcessInfo>& infoList) override;

            // throws daq::es::ESInterface::CannotNotify
            void dbReloadInit() override;

            // throws daq::es::ESInterface::CannotNotify
            void dbReloadDone() override;

            // throws daq::es::ESInterface::CannotNotify
            void reset() override;

            // throws daq::es::ESInterface::CannotNotify
            void transitionDone(const std::string& transitionName, const std::string& newState) override;

        protected:
            static ::es::rc::TestResult convert(const daq::tmgr::TestResult& ret);
            static void fillInfo(const ProcessInfo& from, ::es::rc::ProcessInformation& to);
            static void fillInfo(const StateInfo& from, ::es::rc::StateInformation& to);

        private:
            std::unique_ptr<daq::es::internal::Utils> m_util;
    };
}

namespace k8s {
    class RemoteES : public ExpertSystemInterface {
        public:
            RemoteES(const std::string& partitionName, const std::string& esName);

            // throws daq::es::ESInterface::CannotNotify
            void nodeUpdate(const k8s::node::NodeInfo&) override;

            // throws daq::es::ESInterface::CannotNotify
            void workloadUpdate(const k8s::workload::WorkloadInfo&) override;

            // throws daq::es::ESInterface::CannotNotify
            void podUpdate(const k8s::pod::PodInfo&) override;

            // throws daq::es::ESInterface::CannotNotify
            void containerUpdate(const k8s::container::ContainerInfo&) override;

        protected:
            static ::es::k8s::ConditionStatus convert(daq::es::k8s::ConditionStatus);
            static ::es::k8s::node::ConditionType convert(daq::es::k8s::node::ConditionType);
            static ::es::k8s::workload::Kind convert(daq::es::k8s::workload::Kind);
            static ::es::k8s::pod::Phase convert(daq::es::k8s::pod::Phase);
            static ::es::k8s::pod::ConditionType convert(daq::es::k8s::pod::ConditionType);
            static ::es::k8s::container::State convert(daq::es::k8s::container::State);
            static ::es::k8s::container::Type convert(daq::es::k8s::container::Type);
            static void fillLabels(const std::map<std::string, std::string>& from, ::es::k8s::Label_List& to);
            static void fillConditions(const std::map<daq::es::k8s::node::ConditionType, daq::es::k8s::node::Condition>& from, ::es::k8s::node::Condition_List& to);
            static void fillConditions(const std::map<std::string, daq::es::k8s::workload::Condition>& from, ::es::k8s::workload::Condition_List& to);
            static void fillConditions(const std::map<daq::es::k8s::pod::ConditionType, daq::es::k8s::pod::Condition>& from, ::es::k8s::pod::Condition_List& to);

        private:
            std::unique_ptr<daq::es::internal::Utils> m_util;
    };
}

namespace internal {
    class Utils {
        friend class daq::es::rc::RemoteES;
        friend class daq::es::k8s::RemoteES;

        private:
            Utils(const std::string& partition, const std::string& es);
            ::es::ExpertSystem_var lookup();

            const IPCPartition m_partition;
            const std::string m_es_name;
    };
}
}}


#endif /* DAQ_ES_REMOTEES_H_ */
