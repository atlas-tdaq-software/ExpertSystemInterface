/*
 * ExpertSystemInterface.h
 *
 *  Created on: May 28, 2013
 *      Author: avolio
 */

#ifndef DAQ_ES_EXPERTSYSTEMINTERFACE_H_
#define DAQ_ES_EXPERTSYSTEMINTERFACE_H_

#include <dvs/action.h>
#include <tmgr/tmresult.h>

#include <string>
#include <vector>
#include <memory>
#include <map>


namespace daq { namespace es {

namespace rc {
    struct Transition {
        std::string m_uid;
        std::string m_command;
        std::string m_source_state;
        std::string m_destination_state;
    };

    struct Command {
        std::string m_uid;
        std::string m_name;
        std::vector<std::string> m_arguments;
    };

    struct ParentInformation {
        bool m_exiting;
        bool m_transitioning;
        bool m_initializing;
        std::string m_fsm_state;
        Transition m_current_transition;
        Transition m_last_transition;
    };

    struct ProcessInfo {
        std::string m_status;
        std::string m_running_host;
        bool m_membership;
        bool m_restarting;
        bool m_not_responding;
        long long m_exit_code;
        long long m_exit_signal;
        long long m_info_time;
        ParentInformation m_parent_info;
    };

    struct StateInfo {
        std::string m_fsm_state;
        Transition m_last_transition;
        Transition m_current_transition;
        long long m_last_transition_time;
        Command m_last_command;
        bool m_internal_error;
        bool m_busy;
        long long m_info_time;
        ParentInformation m_parent_info;
    };

    struct ComponentTestInfo {
        std::string m_component_name;
        daq::tmgr::TestResult m_test_result;
        std::vector<daq::dvs::FailureAction> m_actions;
    };

    struct TestInfo {
        daq::tmgr::TestResult m_global_result;
        std::vector<std::shared_ptr<ComponentTestInfo>> m_components;
    };

    class ExpertSystemInterface {
        public:
            virtual ~ExpertSystemInterface() noexcept {;}

            virtual void applicationUpdate(const std::string& appName, const ProcessInfo& info) = 0;
            virtual void applicationUpdate(const std::string& appName, const StateInfo& info) = 0;
            virtual void applicationUpdate(const std::string& appName, const TestInfo& info) = 0;
            virtual void applicationUpdate(const std::map<std::string, StateInfo>& infoList) = 0;
            virtual void applicationUpdate(const std::map<std::string, ProcessInfo>& infoList) = 0;
            virtual void dbReloadInit() = 0;
            virtual void dbReloadDone() = 0;
            virtual void reset() = 0;
            virtual void transitionDone(const std::string& transitionName, const std::string& newState) = 0;
    };
}

namespace k8s {
    enum class ConditionStatus : unsigned int {
        TRUE,
        FALSE,
        UNKNOWN,
        OTHER
    };

    namespace node {
        enum class ConditionType : unsigned int {
            READY,
            DISK_PRESSURE,
            MEMORY_PRESSURE,
            PID_PRESSURE,
            NETWORK_UNAVAILABLE,
            OTHER
        };

        struct Condition {
            ConditionType m_type;
            ConditionStatus m_status;
            std::string m_reason;
            std::string m_message;
            long long m_time;
        };

        struct NodeInfo {
            std::string m_name;
            bool m_enabled;
            std::map<ConditionType, Condition> m_conditions;
            long long m_info_time;
        };
    }

    namespace workload {
        enum class Kind : unsigned int {
            DEPLOYMENT,
            DAEMONSET
        };

        struct Condition {
            std::string m_type;
            ConditionStatus m_status;
            std::string m_reason;
            std::string m_message;
            long long m_time;
        };

        struct WorkloadInfo {
            Kind m_kind;
            std::string m_name;
            std::string m_namespace;
            std::map<std::string, std::string> m_labels;
            long long m_desired_replicas;
            long long m_ready_replicas;
            long long m_available_replicas;
            bool m_condition_available;
            std::map<std::string, Condition> m_conditions;
            long long m_info_time;
        };
    }

    namespace pod {
        enum class Phase : unsigned int {
            PENDING,
            RUNNING,
            SUCCEEDED,
            FAILED,
            UNKNOWN,
            OTHER
        };

        enum class ConditionType : unsigned int {
            POD_SCHEDULED,
            POD_READY_TO_START_CONTAINERS,
            CONTAINERS_READY,
            INITIALIZED,
            READY,
            OTHER
        };

        struct Condition {
            std::string m_reason;
            ConditionStatus m_status;
            ConditionType m_type;
            std::string m_message;
            long long m_time;
        };

        struct PodInfo {
            std::string m_name;
            std::string m_host;
            std::string m_namespace;
            k8s::workload::WorkloadInfo m_belongsTo;
            std::map<std::string, std::string> m_labels;
            Phase m_phase;
            std::map<ConditionType, Condition> m_conditions;
            long long m_info_time;
        };
    }

    namespace container {
        enum class State : unsigned int {
            WAITING,
            RUNNING,
            TERMINATED,
            OTHER
        };

        enum class Type : unsigned int {
            INIT,
            MAIN,
            EPHEMERAL
        };

        struct ContainerInfo {
            Type m_type;
            k8s::pod::PodInfo m_belongsTo;
            std::string m_name;
            std::string m_host;
            std::string m_namespace;
            State m_state;
            bool m_ready;
            bool m_started;
            long long m_restart_count;
            long long m_exit_code;
            long long m_signal;
            long long m_info_time;
        };
    }

    class ExpertSystemInterface {
        public:
            virtual ~ExpertSystemInterface() noexcept {;}

            virtual void nodeUpdate(const k8s::node::NodeInfo&) = 0;
            virtual void workloadUpdate(const k8s::workload::WorkloadInfo&) = 0;
            virtual void podUpdate(const k8s::pod::PodInfo&) = 0;
            virtual void containerUpdate(const k8s::container::ContainerInfo&) = 0;
    };
}

}}


#endif /* DAQ_ES_EXPERTSYSTEMINTERFACE_H_ */
