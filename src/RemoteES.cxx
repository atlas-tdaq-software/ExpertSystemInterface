/*
 * RemoteES.cxx
 *
 *  Created on: May 28, 2013
 *      Author: avolio
 */

#include "ExpertSystemInterface/RemoteES.h"
#include "ExpertSystemInterface/Exceptions.h"

#include <ipc/exceptions.h>
#include <dal/Computer.h>
#include <dal/ComputerProgram.h>

#include <map>
#include <memory>
#include <mutex>


namespace daq { namespace es {

std::string centralESName() {
    return std::string(::es::centralES);
}

namespace internal {
     Utils::Utils(const std::string& partition, const std::string& es)
        : m_partition(partition), m_es_name(es)
     {
     }

    // throws daq::es::ESInterface::CannotNotify
    ::es::ExpertSystem_var Utils::lookup() {
        try {
            return m_partition.lookup< ::es::ExpertSystem>(m_es_name);
        }
        catch(daq::ipc::Exception& ex) {
            throw daq::es::ESInterface::CannotNotify(ERS_HERE, m_es_name, "some error occurred looking for it in the IPC", ex);
        }
    }

}

namespace rc {
    RemoteES::RemoteES(const std::string& partitionName, const std::string& esName)
        : ExpertSystemInterface(), m_util(new daq::es::internal::Utils(partitionName, esName))
    {
    }

    // throws daq::es::ESInterface::CannotNotify
    void RemoteES::applicationUpdate(const std::string& appName, const ProcessInfo& info) {
        try {
            ::es::rc::ProcessInformation procInfo;
            RemoteES::fillInfo(info, procInfo);

            m_util->lookup()->applicationProcessUpdate(appName.c_str(), procInfo);
        }
        catch(CORBA::Exception& ex) {
            const std::string reason = std::string("a communication problem (\"") + ex._name() + "\") occurred";
            throw daq::es::ESInterface::CannotNotify(ERS_HERE, m_util->m_es_name, reason);
        }
    }

    // throws daq::es::ESInterface::CannotNotify
    void RemoteES::applicationUpdate(const std::string& appName, const StateInfo& info) {
        try {
            ::es::rc::StateInformation stateInfo;
            RemoteES::fillInfo(info, stateInfo);

            m_util->lookup()->applicationStateUpdate(appName.c_str(), stateInfo);
        }
        catch(CORBA::Exception& ex) {
            const std::string reason = std::string("a communication problem (\"") + ex._name() + "\") occurred";
            throw daq::es::ESInterface::CannotNotify(ERS_HERE, m_util->m_es_name, reason);
        }
    }

    // throws daq::es::ESInterface::CannotNotify
    void RemoteES::applicationUpdate(const std::map<std::string, StateInfo>& infoList) {
        try {
            ::es::rc::StateInformation_List allInfo;
            allInfo.length(infoList.size());

            unsigned int idx = 0;
            for(const auto& entry : infoList) {
                allInfo[idx].applicationName = CORBA::string_dup(entry.first.c_str());
                RemoteES::fillInfo(entry.second, allInfo[idx].info);
                ++idx;
            }

            m_util->lookup()->applicationsStateUpdate(allInfo);
        }
        catch(CORBA::Exception& ex) {
            const std::string reason = std::string("a communication problem (\"") + ex._name() + "\") occurred";
            throw daq::es::ESInterface::CannotNotify(ERS_HERE, m_util->m_es_name, reason);
        }
    }

    // throws daq::es::ESInterface::CannotNotify
    void RemoteES::applicationUpdate(const std::map<std::string, ProcessInfo>& infoList) {
        try {
            ::es::rc::ProcessInformation_List allInfo;
            allInfo.length(infoList.size());

            unsigned int idx = 0;
            for(const auto& entry : infoList) {
                allInfo[idx].applicationName = CORBA::string_dup(entry.first.c_str());
                RemoteES::fillInfo(entry.second, allInfo[idx].info);
                ++idx;
            }

            m_util->lookup()->applicationsProcessUpdate(allInfo);
        }
        catch(CORBA::Exception& ex) {
            const std::string reason = std::string("a communication problem (\"") + ex._name() + "\") occurred";
            throw daq::es::ESInterface::CannotNotify(ERS_HERE, m_util->m_es_name, reason);
        }
    }

    // throws daq::es::ESInterface::CannotNotify
    void RemoteES::applicationUpdate(const std::string& appName, const TestInfo& info) {
        try {
            ::es::rc::TestStatusInformation testInfo;

            // The global result
            testInfo.globalResult = RemoteES::convert(info.m_global_result);

            // Set the maximum capacity so that reallocations do not happen
            const auto components_size = info.m_components.size();
            testInfo.components.length(components_size);

            // Now loop over all the components
            for(unsigned int c_idx = 0; c_idx < components_size; ++c_idx) {
                testInfo.components[c_idx].componentName = CORBA::string_dup(info.m_components[c_idx]->m_component_name.c_str());
                testInfo.components[c_idx].result = RemoteES::convert(info.m_components[c_idx]->m_test_result);

                const auto numActions = info.m_components[c_idx]->m_actions.size();
                testInfo.components[c_idx].actions.length(numActions);

                for(std::size_t idx = 0; idx < numActions; ++idx) {
                    testInfo.components[c_idx].actions[idx].name = CORBA::string_dup(info.m_components[c_idx]->m_actions[idx].command.c_str());
                    testInfo.components[c_idx].actions[idx].parameters = CORBA::string_dup(info.m_components[c_idx]->m_actions[idx].parameters_json.c_str());
                    testInfo.components[c_idx].actions[idx].diagnosis = CORBA::string_dup(info.m_components[c_idx]->m_actions[idx].description.c_str());
                    testInfo.components[c_idx].actions[idx].timeout = info.m_components[c_idx]->m_actions[idx].timeout;
                }
            }

            // Notify the Expert System
            m_util->lookup()->applicationTestUpdate(appName.c_str(), testInfo);
        }
        catch(daq::es::ESInterface::CannotNotify& ex) {
            throw ex;
        }
        catch(daq::es::ESInterface::Exception& ex) {
            throw daq::es::ESInterface::CannotNotify(ERS_HERE, m_util->m_es_name, "unexpected error", ex);
        }
        catch(CORBA::Exception& ex) {
            const std::string reason = std::string("a communication problem (\"") + ex._name() + "\") occurred";
            throw daq::es::ESInterface::CannotNotify(ERS_HERE, m_util->m_es_name, reason);
        }
    }

    // throws daq::es::ESInterface::CannotNotify
    void RemoteES::dbReloadInit() {
        try {
            m_util->lookup()->dbReloadInit();
        }
        catch(CORBA::Exception& ex) {
            const std::string reason = std::string("a communication problem (\"") + ex._name() + "\") occurred";
            throw daq::es::ESInterface::CannotNotify(ERS_HERE, m_util->m_es_name, reason);
        }
    }

    // throws daq::es::ESInterface::CannotNotify
    void RemoteES::dbReloadDone() {
        try {
            m_util->lookup()->dbReloadDone();
        }
        catch(CORBA::Exception& ex) {
            const std::string reason = std::string("a communication problem (\"") + ex._name() + "\") occurred";
            throw daq::es::ESInterface::CannotNotify(ERS_HERE, m_util->m_es_name, reason);
        }
    }

    // throws daq::es::ESInterface::CannotNotify
    void RemoteES::reset() {
        try {
            m_util->lookup()->reset();
        }
        catch(CORBA::Exception& ex) {
            const std::string reason = std::string("a communication problem (\"") + ex._name() + "\") occurred";
            throw daq::es::ESInterface::CannotNotify(ERS_HERE, m_util->m_es_name, reason);
        }
    }

    // throws daq::es::ESInterface::CannotNotify
    void RemoteES::transitionDone(const std::string& transitionName, const std::string& newState) {
        try {
            m_util->lookup()->transitionDone(transitionName.c_str(), newState.c_str());
        }
        catch(CORBA::Exception& ex) {
            const std::string reason = std::string("a communication problem (\"") + ex._name() + "\") occurred";
            throw daq::es::ESInterface::CannotNotify(ERS_HERE, m_util->m_es_name, reason);
        }
    }

    ::es::rc::TestResult RemoteES::convert(const daq::tmgr::TestResult& ret) {
        static const std::map<daq::tmgr::TestResult, ::es::rc::TestResult> m = {{daq::tmgr::TmPass, ::es::rc::TestResult::PASSED},
                                                                                {daq::tmgr::TmUndef, ::es::rc::TestResult::UNDEFINED},
                                                                                {daq::tmgr::TmFail, ::es::rc::TestResult::FAILED},
                                                                                {daq::tmgr::TmUnresolved, ::es::rc::TestResult::UNRESOLVED},
                                                                                {daq::tmgr::TmUntested, ::es::rc::TestResult::UNTESTED},
                                                                                {daq::tmgr::TmUnsupported, ::es::rc::TestResult::UNSUPPORTED}};

        const auto& it = m.find(ret);
        if(it != m.end()) {
            return it->second;
        }

        throw daq::es::ESInterface::Exception(ERS_HERE, std::string("The test result \"") + std::to_string(ret) + "\" is unknown");
    }

    void RemoteES::fillInfo(const ProcessInfo& from, ::es::rc::ProcessInformation& to) {
        to.status = CORBA::string_dup(from.m_status.c_str());
        to.runningHost = CORBA::string_dup(from.m_running_host.c_str());
        to.membership = from.m_membership;
        to.isRestarting = from.m_restarting;
        to.isNotResponding = from.m_not_responding;
        to.exitCode = from.m_exit_code;
        to.exitSignal = from.m_exit_signal;
        to.infoTime = from.m_info_time;

        to.parentInfo.isExiting = from.m_parent_info.m_exiting;
        to.parentInfo.isTransitioning = from.m_parent_info.m_transitioning;
        to.parentInfo.isInitializing = from.m_parent_info.m_initializing;
        to.parentInfo.fsmState = CORBA::string_dup(from.m_parent_info.m_fsm_state.c_str());

        to.parentInfo.currentTransition.uid = CORBA::string_dup(from.m_parent_info.m_current_transition.m_uid.c_str());
        to.parentInfo.currentTransition.command = CORBA::string_dup(from.m_parent_info.m_current_transition.m_command.c_str());
        to.parentInfo.currentTransition.sourceState = CORBA::string_dup(from.m_parent_info.m_current_transition.m_source_state.c_str());
        to.parentInfo.currentTransition.destinationState = CORBA::string_dup(from.m_parent_info.m_current_transition.m_destination_state.c_str());

        to.parentInfo.lastTransition.uid = CORBA::string_dup(from.m_parent_info.m_last_transition.m_uid.c_str());
        to.parentInfo.lastTransition.command = CORBA::string_dup(from.m_parent_info.m_last_transition.m_command.c_str());
        to.parentInfo.lastTransition.sourceState = CORBA::string_dup(from.m_parent_info.m_last_transition.m_source_state.c_str());
        to.parentInfo.lastTransition.destinationState = CORBA::string_dup(from.m_parent_info.m_last_transition.m_destination_state.c_str());
    }

    void RemoteES::fillInfo(const StateInfo& from, ::es::rc::StateInformation& to) {
        to.isBusy = from.m_busy;
        to.fsmState = CORBA::string_dup(from.m_fsm_state.c_str());

        to.lastTransition.uid = CORBA::string_dup(from.m_last_transition.m_uid.c_str());
        to.lastTransition.command = CORBA::string_dup(from.m_last_transition.m_command.c_str());
        to.lastTransition.sourceState = CORBA::string_dup(from.m_last_transition.m_source_state.c_str());
        to.lastTransition.destinationState = CORBA::string_dup(from.m_last_transition.m_destination_state.c_str());

        to.currentTransition.uid = CORBA::string_dup(from.m_current_transition.m_uid.c_str());
        to.currentTransition.command = CORBA::string_dup(from.m_current_transition.m_command.c_str());
        to.currentTransition.sourceState = CORBA::string_dup(from.m_current_transition.m_source_state.c_str());
        to.currentTransition.destinationState = CORBA::string_dup(from.m_current_transition.m_destination_state.c_str());

        to.lastCommand.uid = CORBA::string_dup(from.m_last_command.m_uid.c_str());
        to.lastCommand.name = CORBA::string_dup(from.m_last_command.m_name.c_str());
        to.lastCommand.arguments.length(from.m_last_command.m_arguments.size());
        for(unsigned long i = 0; i < to.lastCommand.arguments.length(); ++i) {
            to.lastCommand.arguments[i] = CORBA::string_dup(from.m_last_command.m_arguments[i].c_str());
        }

        to.isInternalError = from.m_internal_error;
        to.lastTransitionTime = from.m_last_transition_time;
        to.infoTime = from.m_info_time;

        to.parentInfo.isExiting = from.m_parent_info.m_exiting;
        to.parentInfo.isTransitioning = from.m_parent_info.m_transitioning;
        to.parentInfo.isInitializing = from.m_parent_info.m_initializing;
        to.parentInfo.fsmState = CORBA::string_dup(from.m_parent_info.m_fsm_state.c_str());

        to.parentInfo.currentTransition.uid = CORBA::string_dup(from.m_parent_info.m_current_transition.m_uid.c_str());
        to.parentInfo.currentTransition.command = CORBA::string_dup(from.m_parent_info.m_current_transition.m_command.c_str());
        to.parentInfo.currentTransition.sourceState = CORBA::string_dup(from.m_parent_info.m_current_transition.m_source_state.c_str());
        to.parentInfo.currentTransition.destinationState = CORBA::string_dup(from.m_parent_info.m_current_transition.m_destination_state.c_str());

        to.parentInfo.lastTransition.uid = CORBA::string_dup(from.m_parent_info.m_last_transition.m_uid.c_str());
        to.parentInfo.lastTransition.command = CORBA::string_dup(from.m_parent_info.m_last_transition.m_command.c_str());
        to.parentInfo.lastTransition.sourceState = CORBA::string_dup(from.m_parent_info.m_last_transition.m_source_state.c_str());
        to.parentInfo.lastTransition.destinationState = CORBA::string_dup(from.m_parent_info.m_last_transition.m_destination_state.c_str());
    }
}

namespace k8s {
    RemoteES::RemoteES(const std::string& partitionName, const std::string& esName)
        : ExpertSystemInterface(), m_util(new daq::es::internal::Utils(partitionName, esName))
    {
    }

    void RemoteES::nodeUpdate(const k8s::node::NodeInfo& nodeInfo) {
        try {
            ::es::k8s::node::NodeInfo info { CORBA::string_dup(nodeInfo.m_name.c_str()),
                                             nodeInfo.m_enabled,
                                             ::es::k8s::node::Condition_List(),
                                             nodeInfo.m_info_time };

            RemoteES::fillConditions(nodeInfo.m_conditions, info.conditions);

            m_util->lookup()->nodeUpdate(info);
        }
        catch(CORBA::Exception& ex) {
            const std::string reason = std::string("a communication problem (\"") + ex._name() + "\") occurred";
            throw daq::es::ESInterface::CannotNotify(ERS_HERE, m_util->m_es_name, reason);
        }
    }

    void RemoteES::workloadUpdate(const k8s::workload::WorkloadInfo& workloadInfo) {
        try {
            ::es::k8s::workload::WorkloadInfo info { RemoteES::convert(workloadInfo.m_kind),
                                                     CORBA::string_dup(workloadInfo.m_name.c_str()),
                                                     CORBA::string_dup(workloadInfo.m_namespace.c_str()),
                                                     ::es::k8s::Label_List(),
                                                     workloadInfo.m_desired_replicas,
                                                     workloadInfo.m_ready_replicas,
                                                     workloadInfo.m_available_replicas,
                                                     workloadInfo.m_condition_available,
                                                     ::es::k8s::workload::Condition_List(),
                                                     workloadInfo.m_info_time};

            RemoteES::fillLabels(workloadInfo.m_labels, info.labels);
            RemoteES::fillConditions(workloadInfo.m_conditions, info.conditions);

            m_util->lookup()->workloadUpdate(info);
        }
        catch(CORBA::Exception& ex) {
            const std::string reason = std::string("a communication problem (\"") + ex._name() + "\") occurred";
            throw daq::es::ESInterface::CannotNotify(ERS_HERE, m_util->m_es_name, reason);
        }
    }

    void RemoteES::podUpdate(const k8s::pod::PodInfo& podInfo) {
        try {
            ::es::k8s::pod::PodInfo info { CORBA::string_dup(podInfo.m_name.c_str()),
                                           CORBA::string_dup(podInfo.m_host.c_str()),
                                           CORBA::string_dup(podInfo.m_namespace.c_str()),
                                           { // workload
                                             RemoteES::convert(podInfo.m_belongsTo.m_kind),
                                             CORBA::string_dup(podInfo.m_belongsTo.m_name.c_str()),
                                             CORBA::string_dup(podInfo.m_belongsTo.m_namespace.c_str()),
                                             ::es::k8s::Label_List(),
                                             podInfo.m_belongsTo.m_desired_replicas,
                                             podInfo.m_belongsTo.m_ready_replicas,
                                             podInfo.m_belongsTo.m_available_replicas,
                                             podInfo.m_belongsTo.m_condition_available,
                                             ::es::k8s::workload::Condition_List(),
                                             podInfo.m_belongsTo.m_info_time
                                           }, // end workload
                                           ::es::k8s::Label_List(),
                                           RemoteES::convert(podInfo.m_phase),
                                           ::es::k8s::pod::Condition_List(),
                                           podInfo.m_info_time };

            RemoteES::fillLabels(podInfo.m_belongsTo.m_labels, info.belongsTo.labels);
            RemoteES::fillLabels(podInfo.m_labels, info.labels);

            RemoteES::fillConditions(podInfo.m_belongsTo.m_conditions, info.belongsTo.conditions);
            RemoteES::fillConditions(podInfo.m_conditions, info.conditions);

            m_util->lookup()->podUpdate(info);
        }
        catch(CORBA::Exception& ex) {
            const std::string reason = std::string("a communication problem (\"") + ex._name() + "\") occurred";
            throw daq::es::ESInterface::CannotNotify(ERS_HERE, m_util->m_es_name, reason);
        }

    }

    void RemoteES::containerUpdate(const k8s::container::ContainerInfo& containerInfo) {
        try {
            ::es::k8s::container::ContainerInfo info { RemoteES::convert(containerInfo.m_type),
                                                       { // pod
                                                         CORBA::string_dup(containerInfo.m_belongsTo.m_name.c_str()),
                                                         CORBA::string_dup(containerInfo.m_belongsTo.m_host.c_str()),
                                                         CORBA::string_dup(containerInfo.m_belongsTo.m_namespace.c_str()),
                                                         { // workload
                                                           RemoteES::convert(containerInfo.m_belongsTo.m_belongsTo.m_kind),
                                                           CORBA::string_dup(containerInfo.m_belongsTo.m_belongsTo.m_name.c_str()),
                                                           CORBA::string_dup(containerInfo.m_belongsTo.m_belongsTo.m_namespace.c_str()),
                                                           ::es::k8s::Label_List(),
                                                           containerInfo.m_belongsTo.m_belongsTo.m_desired_replicas,
                                                           containerInfo.m_belongsTo.m_belongsTo.m_ready_replicas,
                                                           containerInfo.m_belongsTo.m_belongsTo.m_available_replicas,
                                                           containerInfo.m_belongsTo.m_belongsTo.m_condition_available,
                                                           ::es::k8s::workload::Condition_List(),
                                                           containerInfo.m_belongsTo.m_belongsTo.m_info_time
                                                         }, // end workload
                                                         ::es::k8s::Label_List(),
                                                         RemoteES::convert(containerInfo.m_belongsTo.m_phase),
                                                         ::es::k8s::pod::Condition_List(),
                                                         containerInfo.m_belongsTo.m_info_time
                                                       }, // end pod
                                                       CORBA::string_dup(containerInfo.m_name.c_str()),
                                                       CORBA::string_dup(containerInfo.m_host.c_str()),
                                                       CORBA::string_dup(containerInfo.m_namespace.c_str()),
                                                       RemoteES::convert(containerInfo.m_state),
                                                       containerInfo.m_ready,
                                                       containerInfo.m_started,
                                                       containerInfo.m_restart_count,
                                                       containerInfo.m_exit_code,
                                                       containerInfo.m_signal,
                                                       containerInfo.m_info_time };

            RemoteES::fillLabels(containerInfo.m_belongsTo.m_labels, info.belongsTo.labels);
            RemoteES::fillLabels(containerInfo.m_belongsTo.m_belongsTo.m_labels, info.belongsTo.belongsTo.labels);

            RemoteES::fillConditions(containerInfo.m_belongsTo.m_belongsTo.m_conditions, info.belongsTo.belongsTo.conditions);
            RemoteES::fillConditions(containerInfo.m_belongsTo.m_conditions, info.belongsTo.conditions);

            m_util->lookup()->containerUpdate(info);
        }
        catch(CORBA::Exception& ex) {
            const std::string reason = std::string("a communication problem (\"") + ex._name() + "\") occurred";
            throw daq::es::ESInterface::CannotNotify(ERS_HERE, m_util->m_es_name, reason);
        }
    }

    ::es::k8s::ConditionStatus RemoteES::convert(daq::es::k8s::ConditionStatus item) {
        static const std::map<daq::es::k8s::ConditionStatus, ::es::k8s::ConditionStatus> m = {{daq::es::k8s::ConditionStatus::TRUE, ::es::k8s::ConditionStatus::TRUE_},
                                                                                              {daq::es::k8s::ConditionStatus::FALSE, ::es::k8s::ConditionStatus::FALSE_},
                                                                                              {daq::es::k8s::ConditionStatus::UNKNOWN, ::es::k8s::ConditionStatus::UNKNOWN},
                                                                                              {daq::es::k8s::ConditionStatus::OTHER, ::es::k8s::ConditionStatus::OTHER}};
        return (m.find(item)->second);
    }

    ::es::k8s::node::ConditionType RemoteES::convert(daq::es::k8s::node::ConditionType item) {
        static const std::map<daq::es::k8s::node::ConditionType, ::es::k8s::node::ConditionType> m = {{daq::es::k8s::node::ConditionType::READY, ::es::k8s::node::ConditionType::READY},
                                                                                                      {daq::es::k8s::node::ConditionType::DISK_PRESSURE, ::es::k8s::node::ConditionType::DISK_PRESSURE},
                                                                                                      {daq::es::k8s::node::ConditionType::MEMORY_PRESSURE, ::es::k8s::node::ConditionType::MEMORY_PRESSURE},
                                                                                                      {daq::es::k8s::node::ConditionType::PID_PRESSURE, ::es::k8s::node::ConditionType::PID_PRESSURE},
                                                                                                      {daq::es::k8s::node::ConditionType::NETWORK_UNAVAILABLE, ::es::k8s::node::ConditionType::NETWORK_UNAVAILABLE},
                                                                                                      {daq::es::k8s::node::ConditionType::OTHER, ::es::k8s::node::ConditionType::OTHER}};
        return (m.find(item)->second);
    }

    ::es::k8s::workload::Kind RemoteES::convert(daq::es::k8s::workload::Kind item) {
        static const std::map<daq::es::k8s::workload::Kind, ::es::k8s::workload::Kind> m = {{daq::es::k8s::workload::Kind::DEPLOYMENT, ::es::k8s::workload::Kind::DEPLOYMENT},
                                                                                            {daq::es::k8s::workload::Kind::DAEMONSET, ::es::k8s::workload::Kind::DAEMONSET}};

        return (m.find(item)->second);
    }

    ::es::k8s::pod::Phase RemoteES::convert(daq::es::k8s::pod::Phase item) {
        static const std::map<daq::es::k8s::pod::Phase, ::es::k8s::pod::Phase> m = {{daq::es::k8s::pod::Phase::PENDING, ::es::k8s::pod::Phase::PENDING},
                                                                                    {daq::es::k8s::pod::Phase::RUNNING, ::es::k8s::pod::Phase::RUNNING},
                                                                                    {daq::es::k8s::pod::Phase::SUCCEEDED, ::es::k8s::pod::Phase::SUCCEEDED},
                                                                                    {daq::es::k8s::pod::Phase::FAILED, ::es::k8s::pod::Phase::FAILED},
                                                                                    {daq::es::k8s::pod::Phase::UNKNOWN, ::es::k8s::pod::Phase::UNKNOWN},
                                                                                    {daq::es::k8s::pod::Phase::OTHER, ::es::k8s::pod::Phase::OTHER_}};

        return (m.find(item)->second);
    }

    ::es::k8s::pod::ConditionType RemoteES::convert(daq::es::k8s::pod::ConditionType item) {
        static const std::map<daq::es::k8s::pod::ConditionType, ::es::k8s::pod::ConditionType> m = {{daq::es::k8s::pod::ConditionType::POD_SCHEDULED, ::es::k8s::pod::ConditionType::POD_SCHEDULED},
                                                                                                    {daq::es::k8s::pod::ConditionType::POD_READY_TO_START_CONTAINERS, ::es::k8s::pod::ConditionType::POD_READY_TO_START_CONTAINERS},
                                                                                                    {daq::es::k8s::pod::ConditionType::CONTAINERS_READY, ::es::k8s::pod::ConditionType::CONTAINERS_READY},
                                                                                                    {daq::es::k8s::pod::ConditionType::INITIALIZED, ::es::k8s::pod::ConditionType::INITIALIZED},
                                                                                                    {daq::es::k8s::pod::ConditionType::READY, ::es::k8s::pod::ConditionType::READY},
                                                                                                    {daq::es::k8s::pod::ConditionType::OTHER, ::es::k8s::pod::ConditionType::OTHER}};

        return (m.find(item)->second);
    }

    ::es::k8s::container::State RemoteES::convert(daq::es::k8s::container::State item) {
        static const std::map<daq::es::k8s::container::State, ::es::k8s::container::State> m = {{daq::es::k8s::container::State::WAITING, ::es::k8s::container::State::WAITING},
                                                                                                {daq::es::k8s::container::State::RUNNING, ::es::k8s::container::State::RUNNING},
                                                                                                {daq::es::k8s::container::State::TERMINATED, ::es::k8s::container::State::TERMINATED},
                                                                                                {daq::es::k8s::container::State::OTHER, ::es::k8s::container::State::OTHER},
        };

        return (m.find(item)->second);
    }

    ::es::k8s::container::Type RemoteES::convert(daq::es::k8s::container::Type item) {
        static const std::map<daq::es::k8s::container::Type, ::es::k8s::container::Type> m = {{daq::es::k8s::container::Type::INIT, ::es::k8s::container::Type::INIT},
                                                                                              {daq::es::k8s::container::Type::MAIN, ::es::k8s::container::Type::MAIN},
                                                                                              {daq::es::k8s::container::Type::EPHEMERAL, ::es::k8s::container::Type::EPHEMERAL}};

        return (m.find(item)->second);
    }

    void RemoteES::fillLabels(const std::map<std::string, std::string>& from, ::es::k8s::Label_List& to) {
        to.length(from.size());

        unsigned int i = 0;
        for(const auto& entry: from) {
            to[i].key = CORBA::string_dup(entry.first.c_str());
            to[i].value = CORBA::string_dup(entry.second.c_str());
            ++i;
        }
    }

    void RemoteES::fillConditions(const std::map<daq::es::k8s::node::ConditionType, daq::es::k8s::node::Condition>& from, ::es::k8s::node::Condition_List& to) {
        to.length(from.size());

        unsigned int i = 0;
        for(const auto& entry: from) {
            to[i].key = RemoteES::convert(entry.first);
            to[i].value = { RemoteES::convert(entry.second.m_type),
                            RemoteES::convert(entry.second.m_status),
                            CORBA::string_dup(entry.second.m_reason.c_str()),
                            CORBA::string_dup(entry.second.m_message.c_str()),
                            entry.second.m_time };
            ++i;
        }
    }

    void RemoteES::fillConditions(const std::map<std::string, daq::es::k8s::workload::Condition>& from, ::es::k8s::workload::Condition_List& to) {
        to.length(from.size());

        unsigned int i = 0;
        for(const auto& entry: from) {
            to[i].key = CORBA::string_dup(entry.first.c_str());
            to[i].value = { CORBA::string_dup(entry.second.m_type.c_str()),
                            RemoteES::convert(entry.second.m_status),
                            CORBA::string_dup(entry.second.m_reason.c_str()),
                            CORBA::string_dup(entry.second.m_message.c_str()),
                            entry.second.m_time };
            ++i;
        }

    }

    void RemoteES::fillConditions(const std::map<daq::es::k8s::pod::ConditionType, daq::es::k8s::pod::Condition>& from, ::es::k8s::pod::Condition_List& to) {
        to.length(from.size());

        unsigned int i = 0;
        for(const auto& entry: from) {
            to[i].key = RemoteES::convert(entry.second.m_type);
            to[i].value = { CORBA::string_dup(entry.second.m_reason.c_str()),
                            RemoteES::convert(entry.second.m_status),
                            RemoteES::convert(entry.second.m_type),
                            CORBA::string_dup(entry.second.m_message.c_str()),
                            entry.second.m_time };
            ++i;
        }

    }
}
}}

